magyarispell (1.7.2-1) UNRELEASED; urgency=medium

  * New upstream version 1.7.2
  * Refresh patches
  * ACK NMU, thanks Holger!

 -- Balint Reczey <rbalint@ubuntu.com>  Sat, 02 Jan 2021 20:32:37 +0100

magyarispell (1.6.1-2.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Fri, 01 Jan 2021 16:47:27 +0100

magyarispell (1.6.1-2) unstable; urgency=medium

  * Build depend on and use gawk where gensub is used (Closes: #757166)
  * Bump Debhelper compat level to 9
  * Suggest libreoffice instead of openoffice.org
  * Modernize packaging using dh
  * Bump standards version without other changes
  * Finalize changelog

 -- Balint Reczey <balint@balintreczey.hu>  Fri, 22 Aug 2014 22:13:04 +0200

magyarispell (1.6.1-1) unstable; urgency=low

  * New upstream release (Closes: #568322)
  * Adopt the package (Closes: #568328)

 -- Balint Reczey <balint@balintreczey.hu>  Fri, 24 Jan 2014 00:52:19 +0100

magyarispell (1.2+repack-2) unstable; urgency=low

  * QA upload.
  * debian/control:
    - Use git address for Vcs-Git.
    - Remove explicit ihungarian home and upstream email info.

 -- Agustin Martin Domingo <agmartin@debian.org>  Thu, 12 Jan 2012 11:20:11 +0100

magyarispell (1.2+repack-1) unstable; urgency=low

  * QA upload.
  * New upstream release (Closes: #476677,#585132). This is a Debian
    package based in Ubuntu package to make comon dependencies
    simpler.
  * debian/changelog:
    - Add missing changelog entries for some Debian uploads.
      Sorted after versions, not dates.
    - Remove obsolete Local Vars section from changelog.
  * debian/control:
    - Set maintainer to Debian QA.
    - Re-add Homepage field.
    - Add Vcs-Browser and Vcs-Git fields.
    - Bump Standards Version.
    - Fix lintian description-synopsis-starts-with-article.
  * debian/{source,patches}/*: Re-add Debian quilt format settings and
    diffs.
    - Add ${misc:Depends}.
    - Remove ancient openoffice.org-updatedicts dependency.
  * Add debian/watch file.
  * debian/myspell-hu.links: Remove obsolete file.
  * debian/myspell-hu.dirs: Do not create ancient myspell dirs.
  * debian/rules:
    - Fix lintian debian-rules-missing-recommended-target build-{arch,indep}.
    - No longer install info-myspell file.
  * debian/copyright:
    - Recoded to UTF-8
    - Updated. Append COPYING.MPL.

 -- Agustin Martin Domingo <agmartin@debian.org>  Wed, 11 Jan 2012 16:52:54 +0100

magyarispell (1.2+repack-0ubuntu1) precise; urgency=low

  * Drop debian/myspell-hu.post{inst,rm} to drop now obsolete
    update-openoffice-dicts call. (LP: #901572)
  * Repack broken orig.tar.gz to not have the debian/ dir.

 -- Martin Pitt <martin.pitt@ubuntu.com>  Thu, 08 Dec 2011 10:26:55 +0100

magyarispell (1.2-0ubuntu2) lucid; urgency=low

  * Dictionary transition.

 -- Chris Cheney <ccheney@ubuntu.com>  Wed, 03 Feb 2010 16:45:00 -0600

magyarispell (1.2-0ubuntu1build1) gutsy; urgency=low

  * No-change upload to enable stack protector.

 -- Kees Cook <kees@ubuntu.com>  Tue, 14 Aug 2007 11:23:36 -0700

magyarispell (1.2-0ubuntu1) gutsy; urgency=low

  * New upstream release
  * debian/compat
    - Bump compat version to make lintian happy
  * debian/control
    - Modify Maintainer value to match Debian-Maintainer-Field Spec
    - Update standards version
    - Update Openoffice.org recommends verson (based on structural changes in
      the dictionary files)
  * debian/rules
    - Install complete latin2 and (not fully complete) utf8 morphological files as well

 -- Aron Sisak <aron@ubuntu.hu>  Sat, 04 Aug 2007 12:49:25 +0200

magyarispell (0.99.4-3) unstable; urgency=low

  * QA upload for switching to new ispell.
  * Ack NMU (closes: #549108).
  * Switch to the `3.0 (quilt)' source format:
    + initial Fix-i2myspell-script.patch with changes introduced in 0.99.4-2.
  * Provide myspell-hu.info-hunspell file.
  * Bump debhelper compat level to 8.
  * Remove any preinst and postinst files (closes: #619279).

  * debian/rules:
    + use `installdeb-myspell --srcdir' instead of manually installing
      the info-myspell file and dictionaries;
    + stop shipping compatibility links in /usr/share/myspell;
    + make use of installdeb-hunspell;
    + switch to debhelper's tiny rules format.

  * debian/control:
    + Standards-Version: 3.9.1;
    + bump (build-)dependencies on dictionaries-common(-dev) to >= 1.10;
    + bump (build-)dependencies on ispell to >= 3.3.02;
    + bump build-dependency on debhelper to >= 8;
    + remove conflicts with ancient version of openoffice.org (lintian);
    + remove ancient conflicts/replaces/provides on myhungarian;
    + remove alternative dependency on openoffice.org-updatedicts;
    + remove article from short description (lintian);
    + sort dependency fields with wrap-and-sort from ubuntu-dev-tools;
    + make myspell-hu package provide hunspell-dictionary and suggest
      libreoffice instead of transitional openoffice.org.

 -- Robert Luberda <robert@debian.org>  Tue, 29 Mar 2011 19:47:09 +0200

magyarispell (0.99.4-2) unstable; urgency=low

  * QA upload.
  * debian/control: Set maintainer to Debian QA Group.
  * bin/i2myspell: Apply patch from Timur Birsh <taem@linukz.org> to
    generate dictionary correctly. (Closes: #591365)

 -- Tim Retout <diocles@debian.org>  Fri, 08 Oct 2010 20:36:04 +0100

magyarispell (0.99.4-1.3) unstable; urgency=low

  * Non-maintainer upload.
  * build with LANG=C to work around build failure (closes: #549047)
    Thanks to Rene Engelhard for the patch.
  * Various lintian housekeeping:
  * Bump debhelper compatibility from 2(!) to 7
  * As a consequence, replace "dh_clean -k" by "dh_prep"
  * Add proper copyright information to debian/copyright
  * While at it, move the information about the new upstream location to
    Homepage: in debian/control
  * Rephrase package descriptions to better fit current recommendations
    from the Developer's Reference
  * Drop dependency on "debconf | debconf-2.0" which is already handled
    by ${misc:Depends}
  * No longer ignore errors from make in debian/rules
  * Drop spellhtml.debhelper.log leftover file in debian/

 -- Christian Perrier <bubulle@debian.org>  Tue, 16 Mar 2010 10:37:07 +0100

magyarispell (0.99.4-1.2) unstable; urgency=low

  * Non-maintainer upload.
  * add hu.{dic,aff} -> hu_HU.{dic,aff} symlink (closes: #373744) and
    remove hu-HU.{dic,aff} link
  * move dict to /usr/share/hunspell and add compat symlinks from
    /usr/share/myspell/dicts (closes: #541926)
  * utf8ize copyright
  * DH_COMPAT -> debian/compat
  * add ${misc:Depends}

 -- Rene Engelhard <rene@debian.org>  Wed, 30 Sep 2009 16:40:04 +0200

magyarispell (0.99.4-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * cdebconf transition: allow the dependency on debconf to be satisfied with
    an alternate of debconf-2.0 (Closes: #332015).

 -- Amaya Rodrigo Sastre <amaya@debian.org>  Mon, 20 Aug 2007 20:11:16 +0200

magyarispell (0.99.4-1) unstable; urgency=high

  * New upstream release
  * Upstream fixed FTBFS bug in bin/igesgen and bin/copydict
  * Coding system is fixed to 8859-2 in ihungarian's info-ispell file
  * Dependencies for the approprriate versions of dictionaries-common
    and dictionaries-common-dev added as in Bugreport #232151 agmartin
    mentioned (closes: #232151)
  * Project Homepage moved to magyarispell.sf.net, so I write this
    change to the copyright file

 -- PASZTOR Gyorgy <pasztor@linux.gyakg.u-szeged.hu>  Thu, 19 Feb 2004 23:47:05 +0000

magyarispell (0.94-2.1) unstable; urgency=low

  * NMU
  * new myspell policy package (closes: #206124)
  * remove full stops from package descriptions

 -- Rene Engelhard <rene@debian.org>  Fri,  8 Aug 2003 12:55:29 +0200

magyarispell (0.94-2) unstable; urgency=low

  * Gunnar's fixes works (closes: #201174)
  * fix debian/rules, to not build spellhtml (closes: #202363)

 -- PASZTOR Gyorgy <pasztor@linux.gyakg.u-szeged.hu>  Tue, 22 Jul 2003 10:37:12 +0200

magyarispell (0.94-1.1) unstable; urgency=low

  * Two scripts did not begin with #!/bin/sh (Closes: 201174)
  * NMU by Gunnar Wolf <gwolf@debian.org>

 -- Gunnar Wolf <gwolf@debian.org>  Fri, 18 Jul 2003 12:15:07 +0200

magyarispell (0.94-1) unstable; urgency=low

  * New upstream release

 -- PASZTOR Gyorgy <pasztor@linux.gyakg.u-szeged.hu>  Tue,  8 Jul 2003 23:01:24 +0200

magyarispell (0.84-1.1) unstable; urgency=low

  * New policy package (closes: #164258)

 -- Agustin Martin Domingo <agmartin@debian.org>  Mon, 28 Oct 2002 16:40:59 +0100

magyarispell (0.84-1) unstable; urgency=low

  * New upstream release

 -- PASZTOR Gyorgy <pasztor@linux.gyakg.u-szeged.hu>  Tue,  4 Jun 2002 09:38:24 +0200

magyarispell (0.83-1) unstable; urgency=low

  * New upstream release

 -- PASZTOR Gyorgy <pasztor@linux.gyakg.u-szeged.hu>  Mon,  3 Jun 2002 15:07:14 +0200

magyarispell (0.82-1) unstable; urgency=low

  * New upstream release

 -- PASZTOR Gyorgy <pasztor@linux.gyakg.u-szeged.hu>  Sun, 19 May 2002 22:45:03 +0200

magyarispell (0.81-1) unstable; urgency=low

  * New upstream release

 -- PASZTOR Gyorgy <pasztor@linux.gyakg.u-szeged.hu>  Fri, 10 May 2002 15:57:48 +0200

magyarispell (0.80-1) unstable; urgency=low

  * New upstream release

 -- PASZTOR Gyorgy <pasztor@linux.gyakg.u-szeged.hu>  Tue, 30 Apr 2002 15:31:30 +0200

magyarispell (0.71-2) unstable; urgency=high

  * Closes: #144144

 -- PASZTOR Gyorgy <pasztor@linux.gyakg.u-szeged.hu>  Tue, 23 Apr 2002 12:05:47 +0200

magyarispell (0.71-1) unstable; urgency=low

  * New upstream release

 -- PASZTOR Gyorgy <pasztor@linux.gyakg.u-szeged.hu>  Thu, 18 Apr 2002 13:51:18 +0200

magyarispell (0.65-1) unstable; urgency=low

  * New upstream release
  * Closes: #141267

 -- PASZTOR Gyorgy <pasztor@debian.fsn.hu>  Mon,  8 Apr 2002 13:53:40 +0200

magyarispell (0.63-1) unstable; urgency=low

  * New upstream release
  * Closes: #128786
  * Upstream author modified the Makefile, as I asked him

 -- PASZTOR Gyorgy <pasztor@linux.gyakg.u-szeged.hu>  Tue, 12 Feb 2002 12:05:42 +0100

magyarispell (0.62-1) unstable; urgency=low

  * New upstream release

 -- PASZTOR Gyorgy <pasztor@linux.gyakg.u-szeged.hu>  Mon, 21 Jan 2002 16:57:22 +0100

magyarispell (0.61-1) unstable; urgency=low

  * Initial Release.

 -- PASZTOR Gyorgy <pasztor@linux.gyakg.u-szeged.hu>  Mon, 21 Jan 2002 13:28:49 +0100


